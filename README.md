## CONTENTS OF THIS FILE

* Introduction
* Installation
* Configuration
* Troubleshooting
* Maintainers

## INTRODUCTION

The Responsive views pager module helps to show different number of items in
views depending on the presenting device.

* For a full description of the module, visit the project page: 
https://www.drupal.org/project/responsive_views_pager

* To submit bug reports and feature suggestions, or to track changes: 
https://www.drupal.org/project/issues/responsive_views_pager


## INSTALLATION

 * Install the module using composer to download the libraries. 
With Drupal 8+ the mobile_detect library is autoloaded into the vendor/ folder.

   `composer require drupal/responsive_views_pager`


## CONFIGURATION

 * The module provides a pager plugin for views:
 * Install module
 * Edit view
 * Choose options for "Use pager" under Pager
 * Select "Paged output, dynamic pager" and apply
 * Click settings and adjust values under "Dynamic items per page"
 * Apply and Save

## MAINTAINERS

Current maintainers:

 * Siju Mathew (sijumpk) - https://www.drupal.org/u/sijumpk
