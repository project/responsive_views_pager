<?php

namespace Drupal\responsive_views_pager\Plugin\views\pager;

use Detection\MobileDetect;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Pager\PagerManagerInterface;
use Drupal\Core\Pager\PagerParametersInterface;
use Drupal\views\Plugin\views\pager\Full;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The plugin to handle responsive pager.
 *
 * @ingroup views_pager_plugins
 *
 * @ViewsPager(
 *   id = "dynamic",
 *   title = @Translation("Paged output, dynamic pager"),
 *   short_title = @Translation("Dynamic"),
 *   help = @Translation("Paged output, dynamic Drupal style"),
 *   theme = "pager",
 *   register_theme = FALSE
 * )
 */
class Dynamic extends Full {

  /**
   * The mobile detector.
   *
   * @var \Detection\MobileDetect
   */
  protected $mobileDetect;

  /**
   * Constructs a SqlBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Pager\PagerManagerInterface $pager_manager
   *   The pager manager.
   * @param \Drupal\Core\Pager\PagerParametersInterface $pager_parameters
   *   The pager parameters.
   * @param \Detection\MobileDetect $mobile_detect
   *   The mobile detector.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, PagerManagerInterface $pager_manager, PagerParametersInterface $pager_parameters, MobileDetect $mobile_detect) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $pager_manager, $pager_parameters);
    $this->mobileDetect = $mobile_detect;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('pager.manager'),
      $container->get('pager.parameters'),
      $container->get('mobile_detect')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['dynamic_items_per_page'] = [
      'contains' => [
        'desktop' => ['default' => 10],
        'mobile' => ['default' => 10],
        'override_mobile' => ['default' => FALSE],
        'tablet' => ['default' => 10],
        'override_tablet' => ['default' => FALSE],
      ],
    ];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $form['items_per_page']['#type'] = 'hidden';
    unset($form['expose']);

    $form['dynamic_items_per_page'] = [
      '#type' => 'fieldset',
      '#tree' => TRUE,
      '#title' => $this->t('Dynamic items per page'),
      '#weight' => -10,
    ];
    $form['dynamic_items_per_page']['desktop'] = [
      '#type' => 'number',
      '#min' => 0,
      '#title' => $this->t('Items per page in desktop (default)'),
      '#description' => $this->t('Enter 0 for no limit.'),
      '#default_value' => $this->options['dynamic_items_per_page']['desktop'],
    ];

    foreach (['mobile', 'tablet'] as $device) {
      $form['dynamic_items_per_page']['override_' . $device] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Override items per page in @device', ['@device' => $device]),
        '#default_value' => $this->options['dynamic_items_per_page']['override_' . $device],
      ];
      $form['dynamic_items_per_page'][$device] = [
        '#type' => 'number',
        '#min' => 0,
        '#description' => $this->t('Enter 0 for no limit.'),
        '#default_value' => $this->options['dynamic_items_per_page'][$device],
        '#states' => [
          'visible' => [
            ':input[name="pager_options[dynamic_items_per_page][override_' . $device . ']"]' => ['checked' => TRUE],
          ],
        ],
      ];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validateOptionsForm(&$form, FormStateInterface $form_state) {
    $desktop_key = ['pager_options', 'dynamic_items_per_page', 'desktop'];
    $form_state->setValue(['pager_options', 'items_per_page'], $form_state->getValue($desktop_key));
  }

  /**
   * {@inheritdoc}
   */
  public function summaryTitle() {
    $titles = [];

    foreach (['desktop', 'mobile', 'tablet'] as $device) {
      if ($device == 'desktop' || $this->options['dynamic_items_per_page']['override_' . $device]) {
        $titles[$device] = $this->formatPlural($this->options['dynamic_items_per_page'][$device],
          '@count item for @device', '@count items for @device', [
            '@count' => $this->options['items_per_page'],
            '@device' => $device,
          ]);
      }
    }
    return $this->t('Paged @titles', ['@titles' => implode(", ", $titles)]);
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    $items_per_page = $this->options['dynamic_items_per_page']['desktop'];

    if ($this->options['dynamic_items_per_page']['override_mobile'] && $this->mobileDetect->isMobile()) {
      $items_per_page = $this->options['dynamic_items_per_page']['mobile'];
    }
    if ($this->options['dynamic_items_per_page']['override_tablet'] && $this->mobileDetect->isTablet()) {
      $items_per_page = $this->options['dynamic_items_per_page']['tablet'];
    }

    $limit = $items_per_page;
    $offset = $this->current_page * $items_per_page + $this->options['offset'];

    if (!empty($this->options['total_pages'])) {
      if ($this->current_page >= $this->options['total_pages']) {
        $limit = $items_per_page;
        $offset = $this->options['total_pages'] * $items_per_page;
      }
    }

    $this->view->query->setLimit($limit);
    $this->view->query->setOffset($offset);
    $this->view->setItemsPerPage($items_per_page);
    $this->view->display_handler->display['cache_metadata']['contexts'][] = 'responsive_views_pager_device';
  }

}
