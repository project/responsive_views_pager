<?php

namespace Drupal\responsive_views_pager\Cache\Context;

use Detection\MobileDetect;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\Context\CacheContextInterface;

/**
 * Defines the 'Responsive Device' cache context.
 *
 * Cache context ID: 'responsive_views_pager_device'.
 */
class ResponsiveDeviceCacheContext implements CacheContextInterface {

  /**
   * The mobile detector.
   *
   * @var \Detection\MobileDetect
   */
  protected $mobileDetect;

  /**
   * Constructs an ResponsiveDeviceCacheContext object.
   *
   * @param \Detection\MobileDetect $mobile_detect
   *   The mobile detector.
   */
  public function __construct(MobileDetect $mobile_detect) {
    $this->mobileDetect = $mobile_detect;
  }

  /**
   * {@inheritdoc}
   */
  public static function getLabel() {
    return 'Responsive Device';
  }

  /**
   * {@inheritdoc}
   */
  public function getContext() {
    if ($this->mobileDetect->isTablet()) {
      return "tablet";
    }
    elseif ($this->mobileDetect->isMobile()) {
      return "mobile";
    }
    return "desktop";
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheableMetadata() {
    return new CacheableMetadata();
  }

}
